## Install

### OSX
`brew cask install osxfuse`

`brew install sshfs`

## Mount
`mkdir -p /tmp/sshmount`


`sudo sshfs -o allow_other,defer_permissions,IdentityFile=<non_relative_private_key_path> ubuntu@<vm_ip>:/home/ubuntu /tmp/sshmount`

You might need to allow additional security permissionsto OSXFuse from the System Preferences app (but you'll probably see a popup anyway)

## Unmount
`sudo diskutil unmount force /tmp/sshmount`
