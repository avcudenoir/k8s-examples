
## Distributed systems

Distributed computing is a field of computer science that studies distributed systems. A distributed system is a system whose components are located on different networked computers, which communicate and coordinate their actions by passing messages to one another.[1] The components interact with one another in order to achieve a common goal.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Distributed-parallel.svg/1024px-Distributed-parallel.svg.png)

Pros:
* low latency
* fault tolerance and redundancy
* scalability / modular growth
* cost effectiveness
* efficiency


Cons:
* complexity
    *  security
* higher initial/development cost
* networking / bandwidth

## Kubernetes

![](docs/images/kubnernetes_1.png)

![](https://d33wubrfki0l68.cloudfront.net/26a177ede4d7b032362289c6fccd448fc4a91174/eb693/images/docs/container_evolution.svg)


## Containers

![](docs/images/container_1)

Linux Containers (LXC) is an operating-system-level virtualization method for running multiple isolated Linux systems (containers) on a single control host (LXC host). It does not provide a virtual machine, but rather provides a virtual environment that has its own CPU, memory, block I/O, network, etc. space and the resource control mechanism. This is provided by namespaces and cgroups features in Linux kernel on LXC host. It is similar to a chroot, but offers much more isolation.

Alternatives for using containers are systemd-nspawn, docker or rkt.




## Pods

Containers are run in Pods which are scheduled to run on Nodes (i.e. worker machines) in a cluster.

Pods run a single replica of an Application and provide:

* Compute Resources (cpu, memory, disk)
* Environment Variables
* Readiness and Health Checking
* Network (IP address shared by containers in the Pod)
* Mounting Shared Configuration and Secrets
* Mounting Storage Volumes
* Initialization


## Workloads

Pods are typically managed by higher level abstractions that handle concerns such as replication, identity, persistent storage, custom scheduling, rolling updates, etc.

The most common out-of-the-box Workload APIs (manage Pods) are:

* Deployments (Stateless Applications)
    replication + rollouts
* StatefulSets (Stateful Applications)
    replication + rollouts + persistent storage + identity
* Jobs (Batch Work)
    run to completion
* CronJobs (Scheduled Batch Work)
    scheduled run to completion
* DaemonSets (Per-Machine)
    per-Node scheduling


## Service Discovery and Load Balancing

Service discovery and Load Balancing may be managed by a Service object. Services provide a single virtual IP address and dns name load balanced to a collection of Pods matching Labels.

Internal vs External Services

* Services Resources (L4) may expose Pods internally within a cluster or externally through an HA proxy.
* Ingress Resources (L7) may expose URI endpoints and route them to Services.



## Configuration and Secrets

Shared Configuration and Secret data may be provided by ConfigMaps and Secrets. This allows Environment Variables, Command Line Arguments and Files to be loosely injected into the Pods and Containers that consume them.

ConfigMaps vs Secrets

* ConfigMaps are for providing non-sensitive data to Pods.
* Secrets are for providing sensitive data to Pods.


## Kubernetes controllers

Controllers

Controllers actuate Kubernetes APIs. They observe the state of the system and look for changes either to desired state of Resources (create, update, delete) or the system (Pod or Node dies).

Controllers then make changes to the cluster to fulfill the intent specified by the user (e.g. in Resource Config) or automation (e.g. changes from Autoscalers).

Example: After a user creates a Deployment, the Deployment Controller will see that the Deployment exists and verify that the corresponding ReplicaSet it expects to find exists. The Controller will see that the ReplicaSet does not exist and will create one.
