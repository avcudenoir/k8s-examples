## Change image, observe rollout, undo it

```bash
kubectl set image deployment/frontend php-redis=gcr.io/google-samples/gb-frontend:v4

kubectl rollout status -w deployment/frontend

kubectl rollout undo deployment/frontend
```



## Force replace, delete and then re-create the resource. Will cause a service outage.
```bash
kubectl replace --force -f ./guestbook-deployment.yaml
```

## Open the guestbook locally

```bash
ssh -L 8080:<svc_ip>:80 ubuntu@<vm_ip> -i <ssh_private_key> -N
```
