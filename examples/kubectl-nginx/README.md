
```bash
kubectl create deployment workshop-nginx --image=nginx # start a single instance of nginx

kubectl get deployments

kubectl get pods

kubectl expose deployment workshop-nginx --type=ClusterIP --name=workshop-nginx-service --port 80

kubectl get service # shorter: kubectl get svc

curl $(kubectl get svc workshop-nginx-service -o jsonpath='{.spec.clusterIP}')

kubectl logs -f deploy/workshop-nginx

kubectl label deployment workshop-nginx new-label=awesome                      # Add a Label

kubectl describe deploy workshop-nginx

kubectl get deploy workshop-nginx -o yaml # -o json

kubectl edit deploy workshop-nginx # deployment/nginx deploy/nginx are also valid

kubectl scale --replicas=3 deploy/workshop-nginx

kubectl run --generator=run-pod/v1 -i --rm --tty test-curl --image=alpine /bin/sh

  # apk add curl
  # curl workshop-nginx-service
  # curl workshop-nginx-service.default
  # curl workshop-nginx-service.default.svc.cluster.local
  # env | grep NGINX
  # curl $WORKSHOP_NGINX_SERVICE_SERVICE_HOST:WORKSHOP_NGINX_SERVICE_SERVICE_PORT

  CTRL+D


kubectl rollout status deploy/workshop-nginx
```



## Cleanup

```bash
kubectl delete deploy/workshop-nginx svc/workshop-nginx-service
```
