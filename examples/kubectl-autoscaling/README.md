```bash
kubectl run --generator=deployment/apps.v1beta1 php-apache --image=k8s.gcr.io/hpa-example --requests=cpu=200m --expose --port=80
```

Now that the server is running, we will create the autoscaler using kubectl autoscale. The following command will create a Horizontal Pod Autoscaler that maintains between 1 and 3 replicas of the Pods controlled by the php-apache deployment we created in the first step of these instructions. Roughly speaking, HPA will increase and decrease the number of replicas (via the deployment) to maintain an average CPU utilization across all Pods of 50%.

Algorithm:

`desiredReplicas = ceil[currentReplicas * ( currentMetricValue / desiredMetricValue )]`


```bash
kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=3

kubectl get hpa

kubectl describe hpa php-apache
```


Now lets increate the load

```bash
kubectl run --generator=run-pod/v1 --rm -i --tty load-generator --image=busybox /bin/sh
...
while true; do wget -q -O- http://php-apache.default.svc.cluster.local; done

```


## Cleanup

```bash
kubectl delete svc/php-apache hpa/php-apache deploy/php-apache
```
