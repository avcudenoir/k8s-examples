#!/usr/bin/env bash

systemctl restart snapd.seeded.service
snap install microk8s --channel=1.14/stable --classic

echo "alias kubectl=\"microk8s.kubectl\"" >> /home/ubuntu/.bashrc
echo "alias k=\"microk8s.kubectl\"" >> /home/ubuntu/.bashrc
echo "alias ka=\"microk8s.kubectl --all-namespaces\"" >> /home/ubuntu/.bashrc
echo "alias kgpa=\"microk8s.kubectl get pods --all-namespaces\"" >> /home/ubuntu/.bashrc
echo "alias kgp=\"microk8s.kubectl get pods\"" >> /home/ubuntu/.bashrc
echo "alias wkgpa=\"watch microk8s.kubectl get pods --all-namespaces\"" >> /home/ubuntu/.bashrc
echo "alias wkgp=\"watch microk8s.kubectl get pods\"" >> /home/ubuntu/.bashrc


/snap/bin/microk8s.status --wait-ready
/snap/bin/microk8s.enable dns
/snap/bin/microk8s.enable storage
/snap/bin/microk8s.enable metrics-server

git clone https://gitlab.com/avcudenoir/k8s-examples.git /home/ubuntu/k8s-examples
chown -R ubuntu:ubuntu /home/ubuntu/k8s-examples
