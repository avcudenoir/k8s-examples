provider "aws" {
  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "jobs-k8s-workshop-23423523413"
    key    = "state"
    region = "eu-central-1"
  }
}
