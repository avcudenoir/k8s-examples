data "aws_region" "current" {}

# Data source to lookup the CIDR block, availability-zones, etc. given the installation subnets
data "aws_subnet" "kubernetes-jobs-workshop" {
  count = "${length(var.subnets)}"
  id    = "${element(var.subnets, count.index)}"
}

data "template_file" "kubernetes-jobs-workshop" {
  template = "${file("${path.module}/files/user-data.sh")}"
}

data "aws_ami" "latest-ubuntu" {
  most_recent = true
  owners      = ["099720109477"] # Canonical

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
