locals {
  # users = ["alan.matuszczak", "aleksander.lorenc", "jakub.jastrzebski", "pawel.szybaj", "sebastian.gorecki", "bartosz.sipiak", "szymon.pietrzak"]

  users = ["alan.matuszczak"]
}

resource "aws_key_pair" "kubernetes-jobs-workshop" {
  key_name   = "kubernetes-jobs-workshop"
  public_key = "${chomp(file("${path.module}/files/ssh/ec2-keypair.pub"))}"
}

resource "aws_launch_configuration" "kubernetes-jobs-workshop" {
  key_name                    = "${aws_key_pair.kubernetes-jobs-workshop.key_name}"
  image_id                    = "${data.aws_ami.latest-ubuntu.id}"
  instance_type               = "t3.medium"
  associate_public_ip_address = true

  security_groups = [
    "${aws_security_group.kubernetes-jobs-workshop.id}",
  ]

  enable_monitoring = true
  user_data         = "${data.template_file.kubernetes-jobs-workshop.rendered}"

  root_block_device = {
    volume_type = "gp2"
    volume_size = "50"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "kubernetes-jobs-workshop" {
  count = "${length(local.users)}"

  name_prefix          = "${local.users[count.index]}-k8s-jobs-workshop-"
  launch_configuration = "${aws_launch_configuration.kubernetes-jobs-workshop.id}"

  availability_zones = [
    "${data.aws_subnet.kubernetes-jobs-workshop.*.availability_zone}",
  ]

  health_check_grace_period = 120

  vpc_zone_identifier = [
    "${var.subnets}",
  ]

  min_size                  = "1"
  max_size                  = "1"
  desired_capacity          = "1"
  force_delete              = "true"
  wait_for_capacity_timeout = "10m"
  health_check_type         = "EC2"

  enabled_metrics = [
    "GroupStandbyInstances",
    "GroupTotalInstances",
    "GroupPendingInstances",
    "GroupTerminatingInstances",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupMinSize",
    "GroupMaxSize",
  ]

  termination_policies = [
    "ClosestToNextInstanceHour",
    "NewestInstance",
  ]

  tag {
    key                 = "Name"
    value               = "${local.users[count.index]}-k8s-jobs-workshop"
    propagate_at_launch = true
  }

  depends_on = [
    "aws_launch_configuration.kubernetes-jobs-workshop",
  ]

  lifecycle {
    create_before_destroy = true
  }
}
